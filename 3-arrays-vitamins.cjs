const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/


//========================================================================================

// 1. Get all items that are available 

function allItemsAvailable(data) {
    const result = data.map(item => {
        return item;
    })
    return result;
}

console.log(allItemsAvailable(items));

//========================================================================================

// 2. Get all items containing only Vitamin C.

function containVitaminC(data) {
    const result = data.filter(item => {
        return item.contains = "Vitamin C";
    });
    return result;
}

console.log(containVitaminC(items));



//========================================================================================

// 3. Get all items containing Vitamin A.

function containVitaminA(data) {
    const result = data.filter(item => {
        return item.contains = "Vitamin A";
    });
    return result;
}

console.log(containVitaminA(items));


//========================================================================================

// 4. Group items based on the Vitamins that they contain in the following format:
// {
//     "Vitamin C": ["Orange", "Mango"],
//     "Vitamin K": ["Mango"],
// }

// and so on for all items and all Vitamins.

function groupItemsBasedOnVitamins(data) {

    const groupedItems = data.reduce((obj, item) => {
        const vitamins = item.contains.split(', ');

        const vita = vitamins.map((vitamin) => {
            if (obj[vitamin] === undefined) {
                obj[vitamin] = [item.name];
            } else {
                obj[vitamin].push(item.name);
            };
            return;
        });

        return obj;

    }, {});

    return groupedItems;
}

console.log(groupItemsBasedOnVitamins(items));


//========================================================================================

// 5. Sort items based on number of Vitamins they contain.

function sortingBasedOnVitamins(data) {
    const result = data.map(item => {
        const numberOfVitamins = item.contains.split(",").length;

        return {
            ...item, numberOfVitamins
        };
    })
        .sort((fruit1, fruit2) => {
            return fruit2.numberOfVitamins - fruit1.numberOfVitamins;
        })


    return result;
}


console.log(sortingBasedOnVitamins(items));




//========================================================================================
